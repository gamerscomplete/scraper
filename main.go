package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/gocolly/colly"
)

const (
	UserAgent  = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36"
	MaxRetries = 3
)

type Post struct {
	Author  string `json:"author"`
	Date    string `json:"date"`
	Content string `json:"content"`
}

type Thread struct {
	Title string  `json:"title"`
	Posts []*Post `json:"posts"`
}

type Progress struct {
	CurrentCategoryURL string `json:"last_category_page_url"`
	Threads            int    `json:"threads"`
	LastThreadIndex    int    `json:"last_thread_index"`
}

type URLSet struct {
	sync.RWMutex
	m map[string]struct{}
}

func NewURLSet() *URLSet {
	return &URLSet{
		m: make(map[string]struct{}),
	}
}

func (s *URLSet) Add(url string) {
	s.Lock()
	defer s.Unlock()
	s.m[url] = struct{}{}
}

func (s *URLSet) Contains(url string) bool {
	s.RLock()
	defer s.RUnlock()
	_, ok := s.m[url]
	return ok
}

func NewCollector() *colly.Collector {
	return colly.NewCollector(
		colly.UserAgent(UserAgent),
		colly.Async(false),
	)
}

func saveProgress(progress *Progress) {
	progressJSON, err := json.MarshalIndent(progress, "", "  ")
	if err != nil {
		log.Fatal(err)
	}

	err = ioutil.WriteFile("progress.json", progressJSON, 0644)
	if err != nil {
		log.Fatal(err)
	}
}

func loadProgress() *Progress {
	jsonData, err := ioutil.ReadFile("progress.json")
	if err != nil {
		return nil
	}

	var progress Progress
	err = json.Unmarshal(jsonData, &progress)
	if err != nil {
		log.Fatal(err)
	}

	return &progress
}

func saveResults(thread *Thread, outputFile string) {
	// Check if the output file exists
	if _, err := os.Stat(outputFile); os.IsNotExist(err) {
		log.Println("Creating a new output file:", outputFile)
		err = ioutil.WriteFile(outputFile, []byte("[]"), 0644)
		if err != nil {
			log.Fatal(err)
		}
	}

	// Read existing data
	jsonData, err := ioutil.ReadFile(outputFile)
	if err != nil {
		log.Fatal(err)
	}

	// Unmarshal data into a slice of threads
	var threads []*Thread
	err = json.Unmarshal(jsonData, &threads)
	if err != nil {
		log.Fatal(err)
	}

	// Append the new thread to the existing threads
	threads = append(threads, thread)

	// Marshal the updated threads slice
	updatedData, err := json.MarshalIndent(threads, "", "  ")
	if err != nil {
		log.Fatal(err)
	}

	// Save updated data to the output file
	err = ioutil.WriteFile(outputFile, updatedData, 0644)
	if err != nil {
		log.Fatal(err)
	}
}

func scrapeTitle(e *colly.HTMLElement, thread *Thread) {
	thread.Title = strings.TrimSpace(e.ChildText("h2.b-post__title"))
}

func scrapePosts(e *colly.HTMLElement, thread *Thread) {
	e.ForEach(".b-userinfo__details", func(_ int, el *colly.HTMLElement) {
		author := strings.TrimSpace(el.ChildText(".author strong a"))

		dateElement := el.DOM.ParentsUntil("body").Find("time[itemprop='dateCreated']")
		date := strings.TrimSpace(dateElement.AttrOr("datetime", ""))

		postContentElement := el.DOM.ParentsUntil(".OLD__post-content").Next().Find(".js-post__content-text")
		postContent, _ := postContentElement.Html()

		post := &Post{
			Author:  author,
			Date:    date,
			Content: strings.TrimSpace(postContent),
		}

		thread.Posts = append(thread.Posts, post)
	})
}

func getNextPageURL(e *colly.HTMLElement) string {
	// Try to find the "next" link in the thread pagination
	nextPageURL := e.ChildAttr("a[rel='next']", "href")

	// If not found, try to find the "next" link in the category pagination
	if nextPageURL == "" {
		nextPageURL = e.ChildAttr("link[rel='next']", "href")
	}

	return nextPageURL
}

func visitNextPage(c *colly.Collector, nextPageURL string) {
	if nextPageURL != "" {
		err := visitWithRetries(c, nextPageURL, MaxRetries)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func visitWithRetries(c *colly.Collector, url string, retries int) error {
	var visitErr error
	for i := 0; i < retries; i++ {
		visitErr = c.Visit(url)
		if visitErr == nil {
			break
		}
		fmt.Println("Failed, attempting retry in ", i+1, " Seconds")
		time.Sleep(time.Duration(i+1) * time.Second)
	}
	return visitErr
}

func main() {
	var categoryURL string
	var maxPages int
	var outputFile string
	var visitedURLsPath string

	flag.StringVar(&outputFile, "output", "output.json", "The output file for saving results")
	flag.StringVar(&visitedURLsPath, "vp", "visited-urls.log", "The output file for saved URL's for resuming")
	flag.StringVar(&categoryURL, "url", "visited_urls.log", "URL of the category to scrape")
	flag.IntVar(&maxPages, "pages", 0, "Number of category pages to fetch (0 for no limit)")
	flag.Parse()

	if categoryURL == "" {
		log.Fatal("Please provide a valid category URL using the -url flag")
	}

	startTime := time.Now()

	visitedURLs, err := loadVisitedURLs(visitedURLsPath)
	if err != nil {
		if os.IsNotExist(err) {
			visitedURLs = make(map[string]struct{})
		} else {
			log.Fatal(err)
		}
	}

	// Load progress from file or initialize a new progress struct
	progress := loadProgress()
	if progress == nil {
		progress = &Progress{
			CurrentCategoryURL: categoryURL,
			Threads:            1,
		}
	}

	threadCount := scrapeThreadsFromCategory(progress.CurrentCategoryURL, maxPages, progress, visitedURLs, outputFile, visitedURLsPath)
	duration := time.Since(startTime)
	averagePagesPerSecond := float64(threadCount) / duration.Seconds()
	fmt.Printf("\nTotal threads archived: %d\n", threadCount)
	fmt.Printf("Time taken: %v\n", duration)
	fmt.Printf("Average pages per second: %.2f\n", averagePagesPerSecond)
}

func scrapeThread(threadURL string) *Thread {
	c := NewCollector()

	var thread Thread

	c.OnHTML("body", func(e *colly.HTMLElement) {
		if thread.Title == "" {
			scrapeTitle(e, &thread)
		}

		scrapePosts(e, &thread)

		nextPageURL := getNextPageURL(e)
		visitNextPage(c, nextPageURL)
	})

	// Visit the first page of the thread
	err := c.Visit(threadURL)
	if err != nil {
		log.Fatal(err)
	}

	return &thread
}

func loadVisitedURLs(path string) (map[string]struct{}, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	visitedURLs := make(map[string]struct{})
	for scanner.Scan() {
		visitedURLs[scanner.Text()] = struct{}{}
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return visitedURLs, nil
}

func scrapeThreadsFromCategory(categoryURL string, maxPages int, progress *Progress, visitedURLs map[string]struct{}, outputFile string, visitedURLsPath string) int {
	c := NewCollector()
	pageCounter := 1
	threadCounter := progress.Threads

	c.OnHTML("body", func(e *colly.HTMLElement) {
		fmt.Printf("Scraping category page %d: %s\n", pageCounter, e.Request.URL.String()) // Output message for category page change
		index := 0
		e.ForEach(".topic-wrapper", func(_ int, el *colly.HTMLElement) {
			if index >= progress.LastThreadIndex {
				sticky := el.ChildText(".prefix")
				if sticky != "Sticky: " {
					threadURL := el.ChildAttr("a.topic-title", "href")
					if _, exists := visitedURLs[threadURL]; !exists && threadURL != "" {
						thread := scrapeThread(threadURL)
						saveResults(thread, outputFile)
						threadCounter++
						progress.Threads++
						progress.LastThreadIndex = index
						visitedURLs[threadURL] = struct{}{}
						saveProgress(progress)

						// Append thread URL to the log file
						f, err := os.OpenFile(visitedURLsPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
						if err != nil {
							log.Fatal(err)
						}
						if _, err := f.WriteString(threadURL + "\n"); err != nil {
							f.Close()
							log.Fatal(err)
						}
						f.Close()

						saveProgress(progress)

						// Output progress message
						fmt.Printf("Archiving thread %d: %s\n", threadCounter, thread.Title)
					}
				}
			}
			index++
		})

		nextPageURL := getNextCategoryPageURL(e)
		progress.LastThreadIndex = 0
		progress.CurrentCategoryURL = nextPageURL
		saveProgress(progress)
		if maxPages == 0 || pageCounter < maxPages {
			pageCounter++
			visitNextPage(c, nextPageURL)
		}
	})

	err := c.Visit(categoryURL)
	if err != nil {
		log.Fatal(err)
	}

	return threadCounter
}

func getNextCategoryPageURL(e *colly.HTMLElement) string {
	return e.DOM.Parent().Find("head > link[rel='next']").AttrOr("href", "")
}
