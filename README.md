# Simple Forum Scraper

This project is a simple forum scraper using GoLang and the Colly web scraping library. It extracts thread titles and posts from given category URLs, saves the results in JSON format, and supports resuming progress in case of failure.

## Build the project
    $ go build

## Use

To use the scraper, run the binary with the following command:

    $ ./forum-scraper -url [CATEGORY_URL] -output [OUTPUT_FILE] -pages [MAX_PAGES]

The arguments are defined as follows:

- url: The URL of the category to scrape.
- output: The output file for saving results (default: output.json).
- pages: The number of category pages to fetch (0 for no limit, default: 0).

## Resume progress

If the scraper is interrupted, it will save its progress in a file called "progress.json". When you run the scraper again, it will automatically resume from where it left off.
